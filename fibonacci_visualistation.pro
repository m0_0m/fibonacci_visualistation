QT += core
QT -= gui

CONFIG += c++11

TARGET = fibonacci_visualistation
CONFIG += console
CONFIG -= app_bundle
LIBS += -lsfml-graphics -lsfml-window -lsfml-system

TEMPLATE = app

SOURCES += main.cpp
