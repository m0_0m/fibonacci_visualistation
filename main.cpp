#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>

const float toRad = 0.0174533f;
// test
unsigned fibonacci(unsigned n);

void secFib(sf::Vertex points[], int N, float R, float alpha, const sf::Vector2f& pos);

int main(){
    sf::RenderWindow window(sf::VideoMode(640, 360), "SFML works!");
    sf::CircleShape shape(1.f);
    sf::Vertex points[24];
    shape.setFillColor(sf::Color::White);
    shape.setPosition(window.getSize().x * 0.5f, window.getSize().y * 0.5f);
    sf::Clock clk;
    while (window.isOpen()){
        sf::Event event;
        while (window.pollEvent(event)){
            if (event.type == sf::Event::Closed)
                window.close();
        }
        if(clk.getElapsedTime().asSeconds()>35) clk.restart();
        window.clear();
        for(int i = 25; i < 50; ++i){
            secFib(points, 24, 0.15f * clk.getElapsedTime().asSeconds() * 0.002f * i, clk.getElapsedTime().asMilliseconds()*0.05f, sf::Vector2f(window.getSize()) * 0.5f);
            window.draw(points, 24, sf::LinesStrip);
        }
        window.display();
    }
    return 0;
}


unsigned fibonacci(unsigned n){
    if(n == 0) return 0;
    if(n == 1) return 1;
    std::vector<unsigned>f(n+1);
    f[0] = 0; f[1] = 1;
    for(unsigned i = 2; i < f.size(); ++i){
        f[i] = f[i-1] + f[i-2];
    }
    return f[n];
}

void secFib(sf::Vertex points[], int N, float R, float alpha, const sf::Vector2f& pos){
    int phi = 0;
    static float L = 0.f;
    for(unsigned i = 0; i < N; ++i){
        ++phi;
        if(phi > 3) phi = 0;
        L = R * fibonacci(i);// длина вектора .position
        points[i].position.x = L * std::cos((90.f * phi - alpha) * toRad);
        points[i].position.y = L * std::sin((90.f * phi - alpha) * toRad);
        switch(phi){
            case 0: points[i].color = sf::Color::Red; break;
            case 1: points[i].color = sf::Color::Yellow; break;
            case 2: points[i].color = sf::Color::Cyan; break;
            case 3: points[i].color = sf::Color::Blue; break;
        }
        points[i].position += pos;
    }
}
